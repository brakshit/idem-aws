import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
async def test_user(hub, ctx):
    # Create IAM user
    user_temp_name = "idem-test-user-" + str(uuid.uuid4())
    tags = [{"Key": "Name", "Value": user_temp_name}]

    # Create IAM user with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.iam.user.present(
        test_ctx, name=user_temp_name, user_name=user_temp_name, tags=tags
    )
    assert ret["result"], ret["comment"]
    assert f"Would create aws.iam.user '{user_temp_name}'" in ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert user_temp_name == resource.get("name")
    assert tags == resource.get("tags")
    assert user_temp_name == resource.get("user_name")

    ret = await hub.states.aws.iam.user.present(
        ctx, name=user_temp_name, user_name=user_temp_name, tags=tags
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert user_temp_name == resource.get("name")
    assert tags == resource.get("tags")
    assert user_temp_name == resource.get("user_name")
    assert "/" == resource.get("path")

    resource_id = resource.get("resource_id")

    # Describe IAM User
    describe_ret = await hub.states.aws.iam.user.describe(ctx)
    assert resource_id in describe_ret

    # Verify that the describe output format is correct
    assert "aws.iam.user.present" in describe_ret.get(resource_id)
    described_resource = describe_ret.get(resource_id).get("aws.iam.user.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert user_temp_name == described_resource_map.get("name")
    assert tags == described_resource_map.get("tags")
    assert user_temp_name == described_resource_map.get("user_name")
    assert "/" == described_resource_map.get("path")
    assert "arn" in described_resource_map

    new_user_name = "idem-test-user-" + str(uuid.uuid4())
    tags.append(
        {
            "Key": f"idem-test-user-key-{str(uuid.uuid4())}",
            "Value": f"idem-test-user-value-{str(uuid.uuid4())}",
        }
    )
    # update with test flag
    ret = await hub.states.aws.iam.user.present(
        test_ctx,
        name=user_temp_name,
        resource_id=resource_id,
        user_name=new_user_name,
        path="/idem/aws/",
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert f"Would update aws.iam.user '{user_temp_name}'" in ret["comment"]
    resource = ret.get("new_state")
    assert user_temp_name == resource.get("name")
    assert tags[0] in resource.get("tags")
    assert tags[1] in resource.get("tags")
    assert new_user_name == resource.get("user_name")
    assert "/idem/aws/" == resource.get("path")

    # test updating only tags with localstack as user_name and path cannot be updated with local stack
    ret = await hub.states.aws.iam.user.present(
        ctx,
        name=user_temp_name,
        resource_id=resource_id,
        user_name=user_temp_name,
        path="/",
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert user_temp_name == resource.get("name")
    assert tags[0] in resource.get("tags")
    assert tags[1] in resource.get("tags")
    assert user_temp_name == resource.get("user_name")
    assert "/" == resource.get("path")
    resource_id = resource.get("resource_id")

    # Localstack is not supporting updating username and path. so using real AWS to test.
    if not hub.tool.utils.is_running_localstack(ctx):
        # test updating user_name and path in real
        ret = await hub.states.aws.iam.user.present(
            ctx,
            name=user_temp_name,
            resource_id=resource_id,
            user_name=new_user_name,
            path="/idem/aws/",
            tags=tags,
        )
        assert ret["result"], ret["comment"]
        assert ret.get("old_state") and ret.get("new_state")
        resource = ret.get("new_state")
        assert user_temp_name == resource.get("name")
        assert tags[0] in resource.get("tags")
        assert tags[1] in resource.get("tags")
        assert new_user_name == resource.get("user_name")
        assert "/idem/aws/" == resource.get("path")
        resource_id = resource.get("resource_id")

    # Delete with test flag
    ret = await hub.states.aws.iam.user.absent(
        test_ctx, name=user_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    assert f"Would delete aws.iam.user '{user_temp_name}'" in ret["comment"]

    # Delete IAM user
    ret = await hub.states.aws.iam.user.absent(
        ctx, name=user_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]

    # Delete IAM user again
    ret = await hub.states.aws.iam.user.absent(
        ctx, name=user_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and not (ret["new_state"])
    assert f"'{user_temp_name}' already absent" in ret["comment"]
