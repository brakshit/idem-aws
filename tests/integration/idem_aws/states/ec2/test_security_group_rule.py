import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.localstack(pro=False)
@pytest.mark.asyncio
async def test_security_group_rule(hub, ctx, aws_ec2_security_group):
    # create security_group rule
    security_group_rule_id = "idem-test-security-group-rule-" + str(uuid.uuid4())
    security_group_id = aws_ec2_security_group.get("resource_id")
    tags = [
        {"Key": "Name", "Value": security_group_rule_id},
    ]
    cidr_ipv4 = "0.0.0.0/0"
    if hub.tool.utils.is_running_localstack(ctx):
        return
    # Create security group rule with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.ec2.security_group_rule.present(
        ctx=test_ctx,
        name=security_group_rule_id,
        group_id=security_group_id,
        ip_protocol="tcp",
        from_port=60,
        to_port=60,
        is_egress=False,
        cidr_ipv4=cidr_ipv4,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert "Would create aws.ec2.security_group_rule" in ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert security_group_rule_id == resource.get("name")
    assert 60 == resource.get("from_port")
    assert 60 == resource.get("to_port")
    assert "tcp" == resource.get("ip_protocol")
    assert tags == resource.get("tags")
    assert cidr_ipv4 == resource.get("cidr_ipv4")

    # Create real security group rule
    ret = await hub.states.aws.ec2.security_group_rule.present(
        ctx=ctx,
        name=security_group_rule_id,
        group_id=security_group_id,
        ip_protocol="tcp",
        from_port=60,
        to_port=60,
        is_egress=False,
        cidr_ipv4="0.0.0.0/0",
        tags=tags,
    )

    resource = ret.get("new_state")
    assert ret["result"], ret["comment"]
    assert tags == resource.get("tags")
    resource_id = resource.get("resource_id")
    assert security_group_rule_id == resource.get("name")
    assert 60 == resource.get("from_port")
    assert 60 == resource.get("to_port")
    assert "tcp" == resource.get("ip_protocol")
    assert tags == resource.get("tags")
    assert cidr_ipv4 == resource.get("cidr_ipv4")

    # modifying the security group rule with test flag
    ret = await hub.states.aws.ec2.security_group_rule.present(
        ctx=test_ctx,
        name=resource_id,
        resource_id=resource_id,
        group_id=security_group_id,
        ip_protocol="tcp",
        from_port=50,
        to_port=50,
        is_egress=False,
        cidr_ipv4="0.0.0.0/0",
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    resource = ret.get("new_state")
    sg_rule = resource.get("security_group_rules")
    assert resource_id == resource.get("resource_id")
    assert 50 == sg_rule[0]["SecurityGroupRule"]["FromPort"]
    assert 50 == sg_rule[0]["SecurityGroupRule"]["ToPort"]
    assert "tcp" == sg_rule[0]["SecurityGroupRule"]["IpProtocol"]
    assert cidr_ipv4 == sg_rule[0]["SecurityGroupRule"]["CidrIpv4"]

    # modifying the security group rule
    ret = await hub.states.aws.ec2.security_group_rule.present(
        ctx=ctx,
        name=resource_id,
        resource_id=resource_id,
        group_id=security_group_id,
        ip_protocol="tcp",
        from_port=50,
        to_port=50,
        is_egress=False,
        cidr_ipv4="0.0.0.0/0",
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert resource_id == ret["name"]
    # Describe Security Group Rule
    describe_ret = await hub.states.aws.ec2.security_group_rule.describe(ctx)
    assert resource_id in describe_ret
    # Verify that describe output format is correct
    assert "aws.ec2.security_group_rule.present" in describe_ret.get(resource_id)
    described_resource = describe_ret.get(resource_id).get(
        "aws.ec2.security_group_rule.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert resource_id == described_resource_map.get("resource_id")
    assert 50 == described_resource_map.get("from_port")
    assert 50 == described_resource_map.get("to_port")
    assert "tcp" == described_resource_map.get("ip_protocol")
    assert tags == described_resource_map.get("tags")
    assert cidr_ipv4 == described_resource_map.get("cidr_ipv4")

    # Delete security group rule with test flag
    ret = await hub.states.aws.ec2.security_group_rule.absent(
        test_ctx,
        name=resource_id,
        group_id=security_group_id,
        resource_id=resource_id,
    )
    assert f"Would delete aws.ec2.security_group_rule" in ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")

    # Delete security group rule
    ret = await hub.states.aws.ec2.security_group_rule.absent(
        ctx,
        name=resource_id,
        group_id=security_group_id,
        resource_id=resource_id,
    )
    assert f"Deleted '{resource_id}'" in ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")

    # Deleting the same security group rule again should state the same in comment
    ret = await hub.states.aws.ec2.security_group_rule.absent(
        ctx,
        name=resource_id,
        group_id=security_group_id,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert (not ret.get("old_state")) and (not ret.get("new_state"))
    assert f"'{resource_id}' is already in deleted state." in ret["comment"]
