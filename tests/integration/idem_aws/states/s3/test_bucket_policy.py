import uuid
from collections import ChainMap

import pytest


@pytest.mark.localstack(pro=False)
@pytest.mark.asyncio
async def test_bucket_policy(hub, ctx, aws_s3_bucket):
    # Create bucket policy
    bucket_policy_temp_name = "idem-test-bucket-policy-" + str(uuid.uuid4())
    bucket_name = aws_s3_bucket.get("Name")
    policy_to_add = (
        '{"Version":"2012-10-17","Statement":[{"Sid":"PublicReadGetObject","Effect":"Allow","Principal":"*","Action":"s3:GetObject","Resource":"arn:aws:s3:::'
        + bucket_name
        + '/*"}]}'
    )
    ret = await hub.states.aws.s3.bucket_policy.present(
        ctx,
        name=bucket_policy_temp_name,
        bucket=bucket_name,
        policy=policy_to_add,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    resource_id = f"{resource.get('Name')}-policy"
    assert policy_to_add == resource.get("policy")

    # Describe s3 bucket policies
    describe_ret = await hub.states.aws.s3.bucket_policy.describe(ctx)
    assert resource_id in describe_ret

    # Verify that describe output format is correct
    assert "aws.s3.bucket_policy.present" in describe_ret.get(resource_id)
    described_resource = describe_ret.get(resource_id).get(
        "aws.s3.bucket_policy.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert "bucket" in described_resource_map
    assert bucket_name == described_resource_map["bucket"]

    assert "policy" in described_resource_map
    assert policy_to_add == described_resource_map["policy"]

    # Delete policy
    ret = await hub.states.aws.s3.bucket_policy.absent(
        ctx, name=resource_id, bucket=aws_s3_bucket.get("Name")
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and ret["new_state"]

    # check if policy is removed
    ret = await hub.states.aws.s3.bucket_policy.absent(
        ctx, name=resource_id, bucket=aws_s3_bucket.get("Name")
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    assert not ret["old_state"].get("policy")
    assert f"Policy is already absent for bucket {bucket_name}" in ret["comment"]
